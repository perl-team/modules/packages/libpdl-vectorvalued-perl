libpdl-vectorvalued-perl (1.0.23-2) UNRELEASED; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.7.0, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 28 Jul 2024 20:19:40 +0200

libpdl-vectorvalued-perl (1.0.23-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Use dh-sequence-pdl for dh_pdl.
  * Enable Salsa CI.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 18 Jun 2024 14:52:39 +0200

libpdl-vectorvalued-perl (1.0.22-1) unstable; urgency=medium

  * Team upload.
  * Bump debhelper compat to 13.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 11 Jun 2023 18:03:24 +0200

libpdl-vectorvalued-perl (1.0.22-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.6.2, no changes.
  * Add Rules-Requires-Root to control file.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 08 Apr 2023 14:45:28 +0200

libpdl-vectorvalued-perl (1.0.21-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Drop PDL 2.080 patch, included upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 30 May 2022 05:36:29 +0200

libpdl-vectorvalued-perl (1.0.20-2) unstable; urgency=medium

  * Team upload.
  * Add patch by Ed J to fix test failures with PDL 2.080.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 29 May 2022 19:17:50 +0200

libpdl-vectorvalued-perl (1.0.20-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 21 May 2022 15:25:57 +0200

libpdl-vectorvalued-perl (1.0.19-1) unstable; urgency=medium

  * Team upload.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 19 May 2022 15:55:13 +0200

libpdl-vectorvalued-perl (1.0.19-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 18 May 2022 17:04:27 +0200

libpdl-vectorvalued-perl (1.0.18-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update copyright file.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 17 Mar 2022 19:05:19 +0100

libpdl-vectorvalued-perl (1.0.17-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 14 Mar 2022 05:36:40 +0100

libpdl-vectorvalued-perl (1.0.16-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 13 Mar 2022 18:56:37 +0100

libpdl-vectorvalued-perl (1.0.15-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 20 Feb 2022 18:05:25 +0100

libpdl-vectorvalued-perl (1.0.14-3) unstable; urgency=medium

  * Team upload.
  * Rebuild with PDL 2.070.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 30 Jan 2022 07:56:25 +0100

libpdl-vectorvalued-perl (1.0.14-2) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Rebuild with PDL 2.061.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on pdl.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 13 Nov 2021 11:25:17 +0100

libpdl-vectorvalued-perl (1.0.14-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 12 Nov 2021 20:39:33 +0100

libpdl-vectorvalued-perl (1.0.13-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 21 Sep 2021 05:54:01 +0200

libpdl-vectorvalued-perl (1.0.12-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update copyright file.
  * Require at least PDL 2.019.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 20 Sep 2021 17:44:08 +0200

libpdl-vectorvalued-perl (1.0.11-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.6.0, no changes.
  * Update upstream metadata.
  * Update copyright file.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 17 Sep 2021 06:33:56 +0200

libpdl-vectorvalued-perl (1.0.10-2) unstable; urgency=medium

  * Team upload.
  * No-change rebuild with PDL 2.057.
    (closes: #993426)

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 05 Sep 2021 06:58:36 +0200

libpdl-vectorvalued-perl (1.0.10-1) unstable; urgency=medium

  * Team upload.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 15 Aug 2021 19:17:05 +0200

libpdl-vectorvalued-perl (1.0.10-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.5.1, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 08 Apr 2021 15:23:24 +0200

libpdl-vectorvalued-perl (1.0.9-2) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.5.0, no changes.
  * Add gbp.conf to use pristine-tar & --source-only-changes by default.
  * Update upstream metadata.

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 19 Nov 2020 08:47:42 +0100

libpdl-vectorvalued-perl (1.0.9-1) unstable; urgency=low

  * Initial release (Closes: #898642).

 -- Laurent Baillet <laurent.baillet@gmail.com>  Fri, 18 May 2018 13:07:03 +0000
